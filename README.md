# Eponymous 4 Recording Management


Welcome to the Eponymous 4 Recording Management repository. There is no code here.

Bitbucket's Wiki and Issue Management interface is ideal in tracking tasks related to writing, recording and mixing Eponymous 4 recordings

## History

This idea is not new. A user on Github is using that service to track home improvement projects.

I tried to use Bugzilla to keep track of the earliest Eponymous 4 recordings, but it went into disuse because Bugzilla turned out to be overkill.

I then tried using MediaWiki, but the experience is not the same.

I want to be able to create a ticket, work on it, then mark it done and leave some sort of trail. But I don't want to host that solution myself.

So, I'm applying the issue tracking paradigm to something that isn't a software project.

Let's see how this goes ...